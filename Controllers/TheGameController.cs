﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core.Models;

namespace Core.Controllers
{
    public class TheGameController : Controller
    {
        // GET: TheGame
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Start(Game game)
        {
            game.PlayerTwoName = "dupa"; 
            return View(game);
        }

        // GET: TheGame/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TheGame/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TheGame/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TheGame/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TheGame/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TheGame/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}