﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Game
    {
        public string PlayerOneName { get; set; }
        public string PlayerTwoName { get; set; }
        public int Angle { get; set; }
        public int Strength { get; set; }
    }
}
