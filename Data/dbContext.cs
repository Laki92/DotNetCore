﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
//using DotNetCore.Entities;

namespace DotNetCore.Models
{
    public class dbContext : DbContext
    {
        public dbContext (DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public DbSet<Core.Entities.Score> Score { get; set; }
        //        public DbSet<Core.Entities.Players> Players { get; set; }
        //        public DbSet<Core.Entities.Weapons> Weapons { get; set; }
    }
}
